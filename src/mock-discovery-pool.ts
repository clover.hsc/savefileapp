import { IDiscoveryPool, IDiscoveryPoolResp, IDeviceAttr, IDrawerType } from './discovery-pool.interface';
import { faker } from '@faker-js/faker';

let mockDiscoveryPoolData: IDiscoveryPool[] = [];

const NUMDevices = 20;
export const AttrSysModel = ['SYS-221BT-DNTR', 'SYS-621BT-HNTR', 'SYS-241H-TNRTTP'];
export const AttrLocation = ['A', 'B', 'C'];
const mockSysSerial = [];

for (let i = 0; i < Math.floor(NUMDevices / 2); i++) {
  mockSysSerial.push(faker.random.alphaNumeric(6));
}

for (let i = 0; i < NUMDevices; i++) {
  let dev: IDiscoveryPool = {} as IDiscoveryPool;

  if (i === 0) {
    dev.state_ui = 'discovered';
    dev.workstation = '0';
    dev.mac = null;
    dev.dt = faker.date.past().toISOString();
    dev.usedby = {};
    dev.type = 'unauth';
    dev.state = 'discovered';
    dev.ipv4 = faker.internet.ip();

    // mock attr information
    dev.attr = {} as IDeviceAttr;
    dev.attr.username = 'ADMIN';
    dev.attr.system_model = '';
    dev.attr.number_of_nodes = '';
    dev.attr.dcms = false;
    dev.attr.location = '';
    dev.attr.rackmount_size = '';
    dev.attr.drawer_type = IDrawerType.None;
    dev.attr.mon = false;
    dev.attr.system_serial = '';
    dev.attr.bios_version = '';
    dev.attr.attr_state = {
      state: 'Unauthorized',
      data: null,
    };
    dev.attr.password = 'ADMIN';
    dev.attr.bmc_version = '';
    dev.attr.product_family = '';
    dev.attr.manufacturer = '';
  } else {
    dev.state_ui = 'discovered';
    dev.workstation = '0';
    dev.mac = faker.internet.mac();
    dev.dt = faker.date.past().toISOString();
    dev.usedby = {};
    dev.type = 'bmc';
    dev.state = 'discovered';
    dev.ipv4 = faker.internet.ip();

    // mock attr information
    dev.attr = {} as IDeviceAttr;
    dev.attr.username = 'ADMIN';
    dev.attr.system_model = AttrSysModel[Math.floor(Math.random() * AttrSysModel.length)];
    dev.attr.number_of_nodes = '2';
    dev.attr.dcms = true;
    dev.attr.location = AttrLocation[Math.floor(Math.random() * AttrLocation.length)];
    dev.attr.rackmount_size = '2';
    dev.attr.drawer_type = IDrawerType.TF;
    dev.attr.mon = false;
    // system_serial should be the same per 2 devices
    dev.attr.system_serial = mockSysSerial[Math.floor(i / 2)];
    dev.attr.bios_version = '1.0a';
    dev.attr.attr_state = {
      state: 'ok',
      data: null,
    };
    dev.attr.password = 'ADMIN';
    dev.attr.bmc_version = '01.00.18';
    dev.attr.product_family = 'Twin Family';
    dev.attr.manufacturer = 'Supermicro';
  }


  mockDiscoveryPoolData.push(dev);
}

export let DiscoveryResp: IDiscoveryPoolResp = {
  dt: faker.date.recent().toISOString(),
  current_page: 1,
  total_page: 1,
  entries: mockDiscoveryPoolData.length,
  list: mockDiscoveryPoolData,
};

/**
 * {
    "reason": "Username or Password invalid",
    "ip_address": "172.24.169.80",
    "result": "Failed"
  }
 */